
package model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 *
 * @author ngonar
 */
@Entity
@Table(name = "pln_nontaglists")
@NamedQueries({
    @NamedQuery(name = "PlnNontaglists.findAll", query = "SELECT p FROM PlnNontaglists p"),
    @NamedQuery(name = "PlnNontaglists.findById", query = "SELECT p FROM PlnNontaglists p WHERE p.id = :id"),
    @NamedQuery(name = "PlnNontaglists.findByTrxNo", query = "SELECT p FROM PlnNontaglists p WHERE p.trxNo = :trxNo"),
    @NamedQuery(name = "PlnNontaglists.findByNoRegistrasi", query = "SELECT p FROM PlnNontaglists p WHERE p.noRegistrasi = :noRegistrasi"),
    @NamedQuery(name = "PlnNontaglists.findByTglRegistrasi", query = "SELECT p FROM PlnNontaglists p WHERE p.tglRegistrasi = :tglRegistrasi"),
    @NamedQuery(name = "PlnNontaglists.findByNama", query = "SELECT p FROM PlnNontaglists p WHERE p.nama = :nama"),
    @NamedQuery(name = "PlnNontaglists.findByIdpel", query = "SELECT p FROM PlnNontaglists p WHERE p.idpel = :idpel"),
    @NamedQuery(name = "PlnNontaglists.findByRptag", query = "SELECT p FROM PlnNontaglists p WHERE p.rptag = :rptag"),
    @NamedQuery(name = "PlnNontaglists.findBySwitchingRef", query = "SELECT p FROM PlnNontaglists p WHERE p.switchingRef = :switchingRef"),
    @NamedQuery(name = "PlnNontaglists.findByAdminCharge", query = "SELECT p FROM PlnNontaglists p WHERE p.adminCharge = :adminCharge"),
    @NamedQuery(name = "PlnNontaglists.findByIso", query = "SELECT p FROM PlnNontaglists p WHERE p.iso = :iso"),
    @NamedQuery(name = "PlnNontaglists.findByXml", query = "SELECT p FROM PlnNontaglists p WHERE p.xml = :xml"),
    @NamedQuery(name = "PlnNontaglists.findByInboxId", query = "SELECT p FROM PlnNontaglists p WHERE p.inboxId = :inboxId")})
@SequenceGenerator(sequenceName="pln_nontaglists_id_seq",name="plnnontaglist_gen", allocationSize=1)
public class PlnNontaglists implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plnnontaglist_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "trx_no")
    private String trxNo;
    @Column(name = "no_registrasi")
    private String noRegistrasi;
    @Column(name = "tgl_registrasi")
    private String tglRegistrasi;
    @Column(name = "nama")
    private String nama;
    @Column(name = "idpel")
    private String idpel;
    @Column(name = "rptag")
    private Double rptag;
    @Column(name = "switching_ref")
    private String switchingRef;
    @Column(name = "admin_charge")
    private Double adminCharge;
    @Column(name = "iso")
    private String iso;
    @Column(name = "xml")
    private String xml;
    @Column(name = "inbox_id")
    private int inboxId;
    @Column(name = "telpon")
    private String telpon;
    @Column(name = "jenis_transaksi")
    private String jenisTransaksi;
    @Column(name = "info")
    private String info;
    @Column(name = "tagihan")
    private String tagihan;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "partner_cid")
    private String partnerCid;
    @Column(name = "merchant")
    private String merchant;
    @Column(name = "pln_ref")
    private String plnRef;
    @Column(name = "bank_code")
    private String bankCode;
    @Column(name = "terminal_id")
    private String terminalId;
    @Column(name = "dt_trx")
    private String dtTrx;
    @Column(name = "reprint")
    private int reprint;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setPartnerCid(String cid) {
        this.partnerCid = cid;
    }

    public String getPartnerCid() {
        return partnerCid;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setPlnRef(String plnRef) {
        this.plnRef = plnRef;
    }

    public String getPlnRef() {
        return plnRef;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setTerminalId(String terminal) {
        this.terminalId = terminal;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setDtTrx(String dtTrx) {
        this.dtTrx = dtTrx;
    }

    public String getDtTrx() {
        return dtTrx;
    }

    public void setReprint(int reprint) {
        this.reprint = reprint;
    }

    public int getReprint() {
        return reprint;
    }

    public PlnNontaglists() {
    }

    public PlnNontaglists(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrxNo() {
        return trxNo;
    }

    public void setTrxNo(String trxNo) {
        this.trxNo = trxNo;
    }

    public String getNoRegistrasi() {
        return noRegistrasi;
    }

    public void setNoRegistrasi(String noRegistrasi) {
        this.noRegistrasi = noRegistrasi;
    }

    public String getTglRegistrasi() {
        return tglRegistrasi;
    }

    public void setTglRegistrasi(String tglRegistrasi) {
        this.tglRegistrasi = tglRegistrasi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public Double getRptag() {
        return rptag;
    }

    public void setRptag(Double rptag) {
        this.rptag = rptag;
    }

    public String getSwitchingRef() {
        return switchingRef;
    }

    public void setSwitchingRef(String switchingRef) {
        this.switchingRef = switchingRef;
    }

    public Double getAdminCharge() {
        return adminCharge;
    }

    public void setAdminCharge(Double adminCharge) {
        this.adminCharge = adminCharge;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public int getInboxId() {
        return inboxId;
    }

    public void setInboxId(int inboxId) {
        this.inboxId = inboxId;
    }

    public String getTelpon() {
        return telpon;
    }

    public void setTelpon(String telpon) {
        this.telpon = telpon;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTagihan() {
        return tagihan;
    }

    public void setTagihan(String tagihan) {
        this.tagihan = tagihan;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenis) {
        this.jenisTransaksi = jenis;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlnNontaglists)) {
            return false;
        }
        PlnNontaglists other = (PlnNontaglists) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PlnNontaglists[id=" + id + "]";
    }

}
